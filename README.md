# Electron-Roof

## 介绍
基于nodeJS而写的Electron桌面程序的模板以及模块(Roof)的示例项目

## 软件架构
基于Node.js写的Electron框架，自定义了Roof模块


## 安装教程🔔

### 1.首先你得确认你的工作环境中安装了 ***Node.js***
```shell
$ node -v
>> v18.14.2
```
### 2.其次你需要确认你是否安装了 ***Electron-builder***
```shell
$ electron-builder --version
>> 24.9.1
```
- 如果你没有安装 *Electron-builder* ，不要担心，请尝试全局安装:
```shell
$ npm i electron-builder -g
# 或
$ npm i electron-builder -D
```
### 3.最后使用命令行 ***`npm i`*** 安装所需的 Electron 模块！
### 4.待安装完成后，你就可以运行项目了~

## 使用说明📣

```shell
# 运行项目
$ npm start

# 打包项目(electron-builder)
$ npm run build
# 将打包在目录下的 `dist` 文件夹里~

# ···
```

## [Roof 模块介绍 `modules/roof`](modules/roof/README.md)
