const Roof = require('./modules/roof');
Roof.init(); // 在渲染进程中引入 Roof 模块并初始化!(必选)

window.addEventListener('DOMContentLoaded', () => {
    for (const type of ['chrome', 'node', 'electron']) {
        console.info(`${type}-版本号：[ ${process.versions[type]} ]`)
    }
}) // 打印输出各个程序的版本号