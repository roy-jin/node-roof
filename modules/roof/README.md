<h1 align="center">Roof</h1>

## 介绍
基于 Electron 的 API 和 NodeJS 而写的模块~

## 如何使用？

### 主进程中的使用

- 在主进程 `main.js` 中，只需要这样导入即可：
```js
const Roof = require('./modules/roof'); 
Roof.init('main'); // 在主进程中引入 Roof 模块并初始化
```
- 运行程序: `npm start`
- Roof 模块会自动帮你打开目录下的 `index.html` 文件~

### 渲染进程中的使用

- 渲染进程， 以 `index.js` 为例
```html
<!-- 在 index.html 中导入渲染进程 -->
<script src="./index.js"></script> 
```
- 在渲染进程 `index.js` 中，你需要这样导入：
```js
const Roof = require('./modules/roof');
Roof.init(); // 在渲染进程中引入 Roof 模块并初始化!(必选)
```

- 你也可以使用 Roof 其他的 API
```js
Roof.Win('close'); // 关闭主窗口
Roof.Win('win'); // 最小化主窗口
Roof.Win('max'); // 最大化主窗口
// ...
```

### 注意：**不要忘了初始化 Roof 模块！！！**

> **如果你不初始化，你将无法正常使用 Roof 模块！**
```js
// 渲染进程初始化
Roof.init();

// 主进程初始化
Roof.init('main');
```

### HTML文档中的使用

- 在HTML文档中，
1. 首先你得引入渲染进程 `index.js`
2. 再在渲染进程里 [*导入并初始化模块*](#渲染进程中的使用)
3. 你就可以在 `body` 里插入代码了，示例：
```html
<body>
    <!-- roof 模块中主要的内容写在 <main> 里! -->
    <roof-main>
        <main>
            <h1> Hello World! </h1>
        </main>
    </roof-main>
</body>
<!-- 导入渲染进程 -->
<script src="./index.js"></script> 
```
> ***注意：*** **Roof 模块的Dom内容请写在 `<roof-main>` 标签里的 `<main>` 元素里！**

## Roof 模块仍在开发中，如果有任何问题，请提交至[**issues**](https://gitee.com/roy-jin/node-roof/issues)！