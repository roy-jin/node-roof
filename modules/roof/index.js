/**
 * Roof 框架的使用
 * 在 Electron 中的主进程中使用 init('main'); 方法初始化模块，渲染进程使用 init();
 */

let { ipcRenderer } = require('electron');

// 添加 Win 方法
function Win(e) {
    switch (e) {
        case 'close': // Roof.Win.('close');
            console.log('Close!');
            ipcRenderer.send('Roof-send', 'close');
            break;
        case 'min': // Roof.Win.('min');
            console.log('Min!');
            ipcRenderer.send('Roof-send', 'min');
            break;
        case 'max': // Roof.Win.('max');
            console.log('Max!');
            ipcRenderer.send('Roof-send', 'max');
            break;
        default:
            console.error(`Roof.Win('${e}'): ${e} is not Found!`);
            break;
    }
}

// 初始化程序
function init(main) {
    if (main === 'main') { // 主程序初始化
        console.log('Roof(Main) is init!');

        const { ipcMain, app, BrowserWindow, globalShortcut } = require('electron');
        const path = require('path')
        let mainWindow;

        // 创建一个窗口
        function createWindow() {
            mainWindow = new BrowserWindow({
                // fullscreen: true, // 全屏状态
                // resizable: false,
                // movable: false,
                icon: 'icons/i.ico', // 程序的图标文件
                minHeight: 500,
                minWidth: 300,
                frame: false, // 关闭默认框架
                transparent: true, // 透明状态
                webPreferences: {
                    nodeIntegration: true,// 这个&↓
                    contextIsolation: false, // 这俩个设置很重要，影响渲染进程的 API 使用
                    // preload: path.join(__dirname,'./preload.js'), // 加入preload的js文件
                    // devTools: false // 禁用Dev调试工具
                }
            })
            mainWindow.loadFile('index.html'); // 加载并打开 index.html 文件
            mainWindow.webContents.openDevTools(); // 自动打开Dev调试框工具(或使用 Ctrl + Shift + I 打开)
        }

        app.on('ready', function () {
            createWindow();
            console.info('# The Start!');
        })

        app.on('activate', function () {
            if (BrowserWindow.getAllWindows().length === 0) createWindow()
        })

        app.on('window-all-closed', function () {
            globalShortcut.unregisterAll();
            if (process.platform !== 'darwin') app.quit()
            console.info('# The End!');
        })
        ipcMain.on('Roof-send', (event, args) => {
            switch (args) {
                case 'close':
                    mainWindow.close();
                    break;
                case 'min':
                    mainWindow.minimize();
                    break;
                case 'max':
                    if (mainWindow.isMaximized()) {
                        mainWindow.unmaximize();
                    } else {
                        mainWindow.maximize();
                    }
                    break;
                default:
                    break;
            }
        })
        return;
    };
    
    // 渲染进程初始化
    console.log('Roof is init!');
    document.write(`<link rel="stylesheet" href="modules/roof/index.css">`);
    class myRoof extends HTMLElement {
        constructor() {
            super();
            this.innerHTML += `<roof-header><a class="close" id="win_close" title="关闭">×</a><a id="win_max" title="最大化">▢</a><a id="win_min" style="font-size: 100%;" title="最小化">—</a></roof-header>`;
            document.querySelector('#win_close').addEventListener('click', () => { Roof.Win('close') })
            document.querySelector('#win_min').addEventListener('click', () => { Roof.Win('min') })
            document.querySelector('#win_max').addEventListener('click', () => { Roof.Win('max') })
        }
    }
    customElements.define('roof-main', myRoof);
};

// 初始化模块，并将方法添加到模块内
module.exports = {
    init,
    Win,
};